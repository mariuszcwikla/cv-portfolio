ifeq ($(OS),Windows_NT)
    #chrome := "/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe"
    chrome := "start chrome"
else
    chrome := google-chrome-stable
endif

# Dlaczego w przypadku Resume używam chrome a nie asciidoctor-pdf?
# Ponieważ asciidoctor-pdf ma wlasny mechanizm layout'owania
# W przypadku Resume layout utworzylem za pomocą CSS'ów (m.in. aby uzyskać "timeline")
# Dlatego drukowanie jest poprzez wygenerowanie html i wydrukowanie do PDF w Chrome

cv:
	asciidoctor -r asciidoctor-pdf -b pdf curriculum-vitae.adoc

resume:
	asciidoctor resume.adoc

# TODO: Drukowanie z command-line nie jest idealne niestety. Drukuje na dwóch stronach
# Zaś drukowanie z poziomu przeglądarki drukuje na jednej stronie.
pdf-resume: resume
	"$(chrome)" --print-to-pdf=$(CURDIR)/resume.pdf $(CURDIR)/resume.html --headless --disable-gpu

pdf: pdf-resume cv
all: pdf

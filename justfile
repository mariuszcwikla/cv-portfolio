set windows-shell := ["cmd.exe", "/c"]

#chrome := "/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe"
chrome := if os_family() == "windows" { "start chrome" } else { "google-chrome-stable" }

# /WAIT jest potrzebne, żeby poczekać na zakończenie tworzenia PDF. W przeciwnym razie Acrobat Reader zablokuje plik
chrome_wait := if os_family() == "windows" { "start /WAIT chrome" } else { "echo NOT IMPLEMENTED!!!" }
cur_dir := if os_family() == "windows" { "%CD%" } else { "$PWD" }

resume-demo:
    @just pdf-resume
#    @just resume
#    {{chrome}} {{cur_dir}}/resume.html
#    @just pdf-resume

# Dlaczego w przypadku Resume używam chrome a nie asciidoctor-pdf?
# Ponieważ asciidoctor-pdf ma wlasny mechanizm layout'owania
# W przypadku Resume layout utworzylem za pomocą CSS'ów (m.in. aby uzyskać "timeline")
# Dlatego drukowanie jest poprzez wygenerowanie html i wydrukowanie do PDF w Chrome

cv:
    asciidoctor -r asciidoctor-pdf -b pdf curriculum-vitae.adoc

resume:
    asciidoctor resume.adoc

# TODO: Drukowanie z command-line nie jest idealne niestety. Drukuje na dwóch stronach
# Zaś drukowanie z poziomu przeglądarki drukuje na jednej stronie.
pdf-resume:
    @just resume
    {{chrome_wait}} --print-to-pdf={{cur_dir}}/resume.pdf {{cur_dir}}/resume.html --headless --disable-gpu
    start resume.pdf

pdf-cv:
    @just cv
    {{chrome_wait}} --print-to-pdf={{cur_dir}}/resume.pdf {{cur_dir}}/resume.html --headless --disable-gpu
    start cv.pdf

pdf: 
    @just pdf-cv
    @just pdf-resume


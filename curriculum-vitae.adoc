:toc: left
:toc-title:

= Mariusz Ćwikła

== About myself

I'm a experienced software developer and designer with 12+ years of commercial experience mostly within Java ecosystem.

My area of expertise is around: software architecture and design, cloud solutions, best practices, automated testing, algorithms, cryptography and security, machine learning, devops and programming languages including (but not only) Java, Ruby, Python, Scala, Groovy.

Also I'm occassional open-source developer.

== Education

+M.+ Sc., Eng. Computer science, spec.: computer systems, Technical University of Rzeszow, 2002-2007 

== Commercial experience

=== March 2022 - Today - Bitbay/Zonda (Software Architect)

TODO

My role as a Software Architect

* architecture design
** designed architecture for the new GraphQL, Federation-based API
** desinged GraphQL business API
** designed Keycloak integration (OAuth2 configuration, security hardening)
** TODO: Onfido; adapting technical aspects to business requirements; refining business requirements
** 
* implementation
** implemented complete pipelines for all required components for GraphQL, Keycloak and Spring-Boot-based backend services
** implementing Spring-Boot based GraphQL services
** implemented Keycloak extensions (custom REST endpoints)
** designed and implemented developer's workflows for working with GraphQL and Keycloak SPIs
** fixed/reported issues in Open-Source libraries that were used in the project
* other technologies used: CockroachDB, Kafka

=== September 2021 - March 2022 - Bitbay/Zonda (Software Engineer)

* developed new Spring-Boot based service 
TODO

* implemented reusable Spring-Boot based libraries:
** Redis (common services)
** MariaDB (common services)
** Kafka (common services)
* introduced Testcontainers library for testing integration with Redis, Kafka, MariaDB
* implemented Jenkins jobs for newly created libraries and microservices
* implemented DCOS deployment scripts for new microservices
* others: OpenFeign

=== November 2020 - August 2021 - Visa (Software Engineer)

Development of Payment Tokenization software:

* main role: software developer
* member of Architect Group
* types of tasks:
** migration of enterprise application: upgraded from Spring 3/Hibernate 3 to Spring 5/Hibernate 5
** implemented and deployed enterprise application to an internal Visa Cloud solution (Kubernetes-based) with Helm and Kubernetes
** implementing customer's requirements
** investigating and fixing JVM performance issues
** improved performance of database queries by redesigning indexes and database queries (e.g. reduced number of indexes from ~15 to ~6)
** designing and improving architecture of whole solution as a member of Architect Group
* Technologies used: Java 8-11, Spring, Hibernate, Docker, Kubernetes, Helm, PostgreSQL, DB2, Oracle, HSQLDB, JUnit, Maven, Jenkins

=== May 2020 - October 2020 - Mobica Ltd (Consultant)

IoT project for US company. Responsible for implementing server-side RESTful API consumed by Android and iOS developers

* Java 11, Wildfly 19 on server side, JPA, MySQL, Docker, TestNG, JUnit
* project run using SCRUM methodology
* making fundamental design decisions as per requirements
* joined Committee of Practice group and took active part in improving system architecture, e.g.:

=== Nov 2019 - April 2020 - Mobica Ltd (Consultant)

Working on a project for English company for the automotive industry.

Main characteristics of the project:

* project was run with SCRUM process, 2 weeks long sprints backed by JIRA
* highly-scalable microservices architecture implemented with Spring Boot, NodeJS (backend), ReactJS on AWS; clustering on AWS Fargate
* CI/CD with bitbucket pipelines and infrastructure-as-code principles
* functional requirements delivered as BDD-style scenarios
* system was consuming vehicle manufacturers API and exposing it's own API
* single team, ~10 engineers

Roles in the project: team/technical leader, software designer, software developer, devops engineer.

Main tasks in the project:

* technical leading by designing, driving the implementation, performing code review
* designing microservices architecture
* designing and developing individual functionalities or components including design of double-layered OAuth2 authentication, logging, tracing, database schema, etc.
* taking active part on business or system requirement gathering and refining
* designing and implementing CI/CD bitbucket pipelines
* designing build system based on Gradle and integrating with CI/CD
* designing and implementing infrastructure with terraform/terragrunt
* designing software delivery process
* managing multiple AWS environments

Tools and frameworks used: AWS (ECS, ECR, VPC, DynamoDB, API Gateway, CloudWatch, Route53), Java, Spring Boot, OAuth2, JWT, HATEOAS, NodeJS, OpenAPI/Swagger, Gradle, Docker, Terragrunt, Bitbucket pipelines, JIRA, Confluence, Lucidcharts, Git, BDD.


=== 2016 - Nov 2019 - Mobica Ltd (Senior Software Developer, Consultant)

Working on payment tokenization system for Dutch company. 

Main characterisitcs of the project:

* system was composed out of 4 main modules (~20 Enterprise Java Applications in total)
* integrated with Android Pay, Samsung Pay, Apple Pay and 3rd party solutions over REST Web services
* on the other end integrated with merchant services over ISO8583 and REST WebService
* 5 teams, each having 5-10 engineers
* strong emphasis on security-related concerns; system was integrated with HSMs (Hardware Security Modules) and built around KMS
* project was run with Scrum-like process and Agile practices backed by JIRA
* frequent releases:
** mutiple releases in progress for different customers
** release model adopted from git-flow
* software deployed on US market

Roles in the project: team/technical leader, software designer, software developer, scrum master.

Main of the tasks performed on project:

* proposing changes to system architecture
* working with business analysts, database administrators, architects, developers and testers to develop specifications or other artifacts
* designing and implementing functionalities including:
** Quartz Jobs for processing large volumes of data
** API endpoints
** integrations over ISO 8583 (including Visa/Mastercard ISO 8583 specifications)
** functionalities backed by various cryptographic algorithms (e.g. SHA, PGP, AES, RSA, HMAC)
** implementing UI using Apache Wicket
* building clustered environments (IBM Websphere)
* analyzing complex issues, including remote debugging on Websphere in edge cases
* PCI-DSS compliance with HP Fortify
* performing code review
* team-leading: running scrum ceremonies
* involved in cross-team release planning process

Tools and frameworks used: Java, Spring, Apache Wicket, Maven, git, Websphere 8.5, Websphere 9, bitbucket, Quartz Jobs, Cryptography algorithms, PCI DSS, HP Fortify, Jenkins, JIRA, OpenAPI

=== March 2016 - June 2016 - Mobica Ltd - (Senior Software Developer)

Development of banking software for Hungarian company.

Roles in the project: software developer.

Tools and frameworks used: Java, Apache Tomcat, Spring, Spring Integration framework, EIP patterns, AngularJS.

=== 2013 - 2016 - Asseco Poland - (Senior Software Designer)

Project #2 (1 month)

Developed a component for another system Java EE based system:

* component was exposed as a REST service
* Hibernate, Spring Data was used to access data
* integrated with IBM ESB. 
* application was developed using Maven and IntelliJ IDEA

Project #1

Design & development of OSGi-based Web application

* responsible for architecture of the solution
* application GUI was created in Eclipse RAP, based on BPMN 2.0
* automated tests in mind
* integrated with multiple backend services: LDAP/Active Directory, REST-based backend, SOAP-based backend
* deployed on Jetty, Tomcat, Weblogic, Websphere, JBoss
* authored official developer’s manual and team blog written in asciidoctor
* responsible for maintaining continuous integration (Jenkins, JUnit) and build system based on Gradle
* performed a static code analysis for security issues using HP Fortify (against OWASP issues)
* a scrum master


=== 2011 - 2013 - Asseco Poland - (Software Designer)

Design & development of the API for existing Asseco banking system

* design was done with UML 2.0 in Sparx Enterprise Architect
* performed reverse engineering of banking system (PL/SQL)
* code written with Java EE 6, on Weblogic application server
* services were exposed as JAX-WS Web services
* system was developed with automation tests in mind (JUnit). 
* project was related to banking industry and run using RUP/Waterfall.

Created plug-ins for Enterprise Architect, including PL/SQL parser (C#, ANTLR) that were used by many developers and designers and by multiple teams.

=== 2007 - 2011 - Asseco Poland - (Software Developer)

Member of the development team that developed loan origination system:

* system was developed in Java EE 1.4 (on OAS and Websphere application servers) and Oracle 10 as database server
* system was build on XPDL-based workflow (workflow engin e developed by Asseco)
* integrated with other Asseco enterprise systems (both as a consumer and producer)
* responsible for development of user interface (Java Server Faces), business logic, database layer and integration layer (JSP, EJB 2.1, SOAP Web Services, CORBA, PL/SQL stored procedures).

Project was run using RUP/Waterfall.


== Cool subprojects

Below is list of cool subprojects (development tools) I developed as part of commercial projects :


[cols='~,70', frame=none, grid=none, stripes=none]
|===

|*HTTP Communication visualiser for JUnit* | Tool for generating PlantUML Sequence diagrams that was showing complex flows/interactions between components. 

Diagrams were generated at the runtime, during JUnit executions.


|*Static code analyzer for EAR applications* |

Static code analyzer written in Java with Apache BCEL library (ByteCode Engineering Library). Tool was scanning multiple Enterprise Applications and was trying to generate "call graphs/dependency graphs". 

Eventually PlantUML Component diagrams were generated.



|*Configuration management plugin for Eclipse RCP*|

Plugin (Java) for Eclipse RCP. It allowed easy management/configuration of OSGi dependencies.



|*PL/SQL Code importer for Sparx Enterprise Architect* |

Plugin written in (C#) for importing PL/SQL code into Sparx Enterprise Architect. For parsing I used ANTLR library.

|===

== Noncommercial experience

=== Open Source

Contributed to following projects:

* Asciidoctor (ruby) - new feature: integration with gnuplot
* JRuby (java, ruby) - bugfixes
* TestContainers (java) - bugfixes

=== 2003-2007 Technical University of Rzeszów

* Academic projects involving C/C++, C#, Java, OpenGL, Lua.


== Training Courses and Certificates

* OMG Certified UML Professional – Fundamental Certification - 
http://www.omg.org/cgi-bin/certdetails.cgi?file=636682-uml-ocup-fundamental.jpg
* Effective system modeling in Enterprise Architect - Modesto training
* Application and system architecture - Design patterns for software designers - Bottega IT solutions training
* Google Professional Cloud Architect - https://www.credential.net/562gxxoc?key=bfb9060ae2a2c396abccbe67b8de83e796e4529d4763a3b4efe6a47f1a69c264

== Me as a speaker

Some trainings I have conducted for co-workers:

* Design Patterns
* Algorithms and Cryptography (series of ~15 trainings)
* Introduction to docker
* Automated testing

== Summary

* Languages: Java, Ruby, Groovy, PL/SQL, Python, C/C++, C#, Javascript, HTML, Typescript, Scala, Lua
* Java EE & Web servers: OAS, Websphere 7, 8, 8.5, JBoss 5, 7, Weblogic 10, 11, Apache Tomcat, Jetty, Wildfly 19
* Database systems: Oracle 10, 11, IBM DB2, H2, HSQLDB, DynamoDB, Cassandra, MongoDB, neo4j
* Cloud Services: AWS, GCP
* IDE&Tools: Eclipse, IntelliJ IDEA, Sparx Enterprise Architect, JDeveloper, IBM RAD, PL/SQL Developer, JIRA, Confluence, HP Fortify, Visual Studio Code, vim
* Other tools & libraries: BPMN, XPDL, UML, JUnit, ANTLR, OSGi, LDAP, Active Directory, Eclipse RCP, Ant, Maven, Gradle, Vaadin, AngularJS, Spring framework, Spring Security, Spring Integration, Apache Wicket, NoSQL technologies, Apache Solr, ActiveMQ, Docker, Jenkins, liquibase, dbunit, Quartz, Terraform, Terragrunt, Asciidoctor, Markdown, Bitbucket, Travis-CI, plantUML, graphviz, gnuplot
* crypto standards or concepts: OAuth2, JWS, JWE, JWT, KMS, OpenID Connect
* other standards: OpenAPI, HATEOAS, REST, SOAP
* Other skills: Tech Lader, Team Leader, Scrum Master; worked closely with Project Managers, Business Analysts and Customers world-wide
* Areas of interests: algorithms, design patterns, object-oriented principles, system design & architecture, cloud, security & cryptography, devops


== Code samples

* https://github.com/jruby/jruby/pull/5915
* https://github.com/asciidoctor/asciidoctor-diagram/pull/241
* https://github.com/jruby/jruby/pull/5953/files
* https://gitlab.com/mariuszcwikla/adventofcode
* https://gitlab.com/mariuszcwikla/ruby-algorithms

== Contact information

* e-mail: mariuszcwikla@gmail.com
* LinkedIn: https://www.linkedin.com/in/mariusz-%C4%87wik%C5%82a-15649a195/
* Github: https://github.com/MariuszCwikla
* Location: Rzeszów, Poland
